# Tinbrow - A minimal web browser in QT and Python, Inspired by qutebrowser.
# *NOTE: THIS BROWSER IS IN ALPHA.*

![Screenshot of the browser](imgs/screenshot.png)
*Screenshot may differ*

## Why?
### Why not.

## TODO:

[X] Fix Fullscreen

[X] Add a menu to change options.

[ ] Add userscripts.

[ ] Add a builtin ad blocker.

[X] Add option to change user agent (to fix some websites like youtube or captcha.).

[X] Add back and forward button.

[X] Fix title and URL Updating.

[X] Add downloading support.

[ ] Fix links not working when you click on them.

[ ] Improve the look.

[ ] Add profiles

[?] Add tabs.
