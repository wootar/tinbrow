#!/usr/bin/env python3
import time
import sys
import os
print("Installing Tinbrow https://github.com/wootar/tinbrow")

progress = ["\\","|","/","-"]

print("(1) Creating directories", end="\r")
try:
    os.system("mkdir /usr &> /dev/null")
    os.system("mkdir /usr/bin &> /dev/null")
    os.system("mkdir /usr/lib &> /dev/null")
    os.system("mkdir /usr/lib/python3 &> /dev/null")
    os.system("mkdir /usr/lib/python3/dist-packages &> /dev/null")
except:
    pass
print("(2) Copying files", end="\r")
try:
    os.system("cp ./tinbrow.py /usr/bin/tinbrow")
    os.system("cp ./tinbrowmain.py /usr/lib/python3/dist-packages/tinbrowmain.py")
except:
    pass
print("", end="\r")
print("Done!", end="\n")