import sys
import time
from PyQt5 import QtCore, QtWebEngineCore, QtWebEngineWidgets, Qt, QtGui, QtWidgets
import platform
import validators

version = "alpha 220707+1"
title = "Tinbrow "+version+" - "
startpage = "about:blank"
pagetitle = "Nothing"

# CHECK IF A ARGUMENT WAS PASSED

if len(sys.argv) > 1:
   url = sys.argv[1]
else:
   url = startpage

# CHECK IF A ARGUMENT WAS PASSED

# Intercepter
class WebEngineUrlRequestInterceptor(QtWebEngineCore.QWebEngineUrlRequestInterceptor):
    def __init__(self, parent=None):
        super().__init__(parent)

    def interceptRequest(self, info):
        print(info.requestUrl())
        return True
# Intercepter

# main class

class main(QtWidgets.QWidget):
   def __init__(self):
      # SETUP
      super().__init__()
      print("Tinbrow "+version)
      self.resize(800,600)
      self.setWindowTitle(title+pagetitle)
      self.fullscreenEnabled = False
      self.mainLayout = QtWidgets.QVBoxLayout()
      self.maintab = QtWidgets.QWidget()
      self.maintab.layout = QtWidgets.QVBoxLayout()
      # SETUP
      # KEYBINDINGS
      self.fullscreenKey = QtWidgets.QShortcut(QtGui.QKeySequence('F11'),self); self.fullscreenKey.activated.connect(self.fullscreen)
      self.fullscreenKey = QtWidgets.QShortcut(QtGui.QKeySequence('F5'),self); self.fullscreenKey.activated.connect(self.refresh)
      # KEYBINDINGS
      # WEBPAGE
      self.web = QtWebEngineWidgets.QWebEngineView()
      self.web.page().profile().setHttpUserAgent("Mozilla/5.0 ("+platform.system()+" "+platform.machine()+") Blink/81.0 Tinbrow/"+version.replace(" ", ""))
      self.web.settings().setAttribute(QtWebEngineWidgets.QWebEngineSettings.FullScreenSupportEnabled, True)
      self.web.page().fullScreenRequested.connect(self.fullscreenToggle)
      intercept = WebEngineUrlRequestInterceptor()
      self.web.page().profile().setUrlRequestInterceptor(intercept)
      self.web.load(Qt.QUrl(url))
      self.web.urlChanged.connect(self.updateUrl)
      self.web.titleChanged.connect(self.updateUrl)
      self.web.page().profile().downloadRequested.connect(self.download)
      # WEBPAGE
      # TOOLBAR
      self.toolbar = QtWidgets.QToolBar("Test")
      self.toolbar.urlbar = QtWidgets.QLineEdit()
      self.toolbar.back = QtWidgets.QPushButton(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_ArrowBack")),"")
      self.toolbar.forward = QtWidgets.QPushButton(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_ArrowForward")),"")
      self.toolbar.refresh = QtWidgets.QPushButton(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_BrowserReload")),"")
      self.toolbar.setStyleSheet("background-color: white; color: black;")
      self.toolbar.urlbar.returnPressed.connect(self.goa)
      self.toolbar.back.clicked.connect(self.back)
      self.toolbar.forward.clicked.connect(self.forward)
      self.toolbar.refresh.clicked.connect(self.refresh)
      # MENU
      # "☰"
      self.toolbar.menubutton = QtWidgets.QPushButton(self.style().standardIcon(getattr(QtWidgets.QStyle, "SP_ToolBarHorizontalExtensionButton")), "")
      self.toolbar.menu = QtWidgets.QMenu()
      self.toolbar.menu.addAction('Refresh', self.refresh)
      self.toolbar.menu.addAction('Userscripts (PLACEHOLDER)', self.passa)
      self.toolbar.menu.addAction('Update Adblocker (PLACEHOLDER)', self.passa)
      self.toolbar.menu.addAction('Use Firefox User Agent for this session', self.firefoxUserAgent)
      self.toolbar.menu.addAction('Use Tinbrow (default) User Agent for this session', self.tinbrowUserAgent)
      self.toolbar.menu.addAction('Clear Browsing Data', self.cleanBrowsing)
      self.toolbar.menu.addAction('Quit', self.quit)
      self.toolbar.menubutton.setMenu(self.toolbar.menu)
      # MENU
      self.toolbar.addWidget(self.toolbar.back)
      self.toolbar.addWidget(self.toolbar.forward)
      self.toolbar.addWidget(self.toolbar.refresh)
      self.toolbar.addWidget(self.toolbar.urlbar)
      self.toolbar.addWidget(self.toolbar.menubutton)
      # TOOLBAR
      # SETUP
      #self.maintab.layout.addWidget(self.toolbar)
      self.maintab.layout.addWidget(self.web)
      self.maintab.setLayout(self.maintab.layout)
      self.mainLayout.addWidget(self.toolbar)
      self.mainLayout.addWidget(self.maintab)
      self.setLayout(self.mainLayout)
      self.total = 0
      self.setStyleSheet("background-color: black; color: white; QTabBar {qproperty-drawBase: 0;};")
      # SETUP
   # GO (GO TO A URL) FUNCTION
   def goa(self):
      url = self.toolbar.urlbar.text()
      pagetitle = str(self.web.title())
      print(Qt.QUrl.isValid(Qt.QUrl(url)))
      if validators.url(str(url)):
         url = url
      else:
         url = "https://" + url
      self.setWindowTitle(title+pagetitle)
      self.web.load(Qt.QUrl(url))  
      self.updateUrl()
   # GO (GO TO A URL) FUNCTION
   # UPDATE URL FUNCTION
   def updateUrl(self):
      url = str(Qt.QUrl.toString(self.web.url()))
      pagetitle = str(self.web.title())
      self.toolbar.urlbar.setText(url)
      self.setWindowTitle(title+pagetitle)
   def refresh(self):
      self.web.reload()
   # UPDATE URL FUNCTION
   # pass function
   def passa(self):
      pass
   def back(self):
      self.web.back()
   def forward(self):
      self.web.forward()
   def download(self, download):
    old_path = download.url().path()
    suffix = QtCore.QFileInfo(old_path).suffix()
    path, _ = QtWidgets.QFileDialog.getSaveFileName(
        self, "Save File", old_path, "*." + suffix
    )
    if path:
        download.setPath(path)
        download.accept()
        download.finished.connect(self.finishNotice)
   def finishNotice(self):
    print("finished")
   # pass function
   # USER AGENT CHANGER FUNCTION
   # Mozilla/5.0 (Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0

   def firefoxUserAgent(self):
      self.web.page().profile().setHttpUserAgent("Mozilla/5.0 ("+platform.system()+" "+platform.machine()+"; rv:102.0) Gecko/20100101 Firefox/102.0")
      self.refresh()
   def tinbrowUserAgent(self):
      self.web.page().profile().setHttpUserAgent("Mozilla/5.0 ("+platform.system()+" "+platform.machine()+") Blink/81.0 Tinbrow/"+version.replace(" ", ""))
      self.refresh()
   # USER AGENT CHANGER FUNCTION
   # CLEAN BROWSING DATA FUNCTION
   def cleanBrowsing(self):
      print("Cleaning visited links")
      self.web.page().profile().clearAllVisitedLinks()
      print("Cleaning HTTP Cache")
      self.web.page().profile().clearHttpCache()
      print("Clearing cookies")
      self.web.page().profile().cookieStore().deleteAllCookies()
      print("Refreshing")
      self.refresh()
   # CLEAN BROWSING DATA FUNCTION
   # QUIT FUNCTION
   def quit(self):
      sys.exit(0)
   # QUIT FUNCTION
   # FULLSCREEN FUNCTIONS
   def fullscreen(self):
      if self.fullscreenEnabled == False:
         self.showFullScreen()
         self.toolbar.toggleViewAction().setChecked(True)
         self.toolbar.toggleViewAction().trigger()
         print("a")
         self.fullscreenEnabled = True
      elif self.fullscreenEnabled == True:
         self.showNormal()
         self.toolbar.toggleViewAction().setChecked(False)
         self.toolbar.toggleViewAction().trigger()
         print('b')
         self.fullscreenEnabled = False
   def fullscreenToggle(self, request):
      if self.fullscreenEnabled == False:
         request.accept()
         self.showFullScreen()
         self.toolbar.toggleViewAction().setChecked(True)
         self.toolbar.toggleViewAction().trigger()
         print("a")
         self.fullscreenEnabled = True
      elif self.fullscreenEnabled == True:
         request.accept()
         self.showNormal()
         self.toolbar.toggleViewAction().setChecked(False)
         self.toolbar.toggleViewAction().trigger()
         print('b')
         self.fullscreenEnabled = False
   # FULLSCREEN FUNCTIONS
# main class