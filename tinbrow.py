#!/usr/bin/env python3
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLineEdit, QLabel, QTableWidget, QVBoxLayout, QGridLayout, QPushButton, QHBoxLayout, QToolBar, QShortcut
from PyQt5.Qt import QUrl
from PyQt5.QtGui import QKeySequence  
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
import time
from PyQt5.QtCore import *
import platform
import tinbrowmain

browser = QApplication(sys.argv)
browser_window = tinbrowmain.main()
browser_window.show()

sys.exit(browser.exec())
