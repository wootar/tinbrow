#!/usr/bin/env python3

import os

print("Installing deps for tinbrow on Debian/Devuan/etc")
if os.system("apt install -y python3-pyqt5.qtwebengine python3-pyqt5 python3-validators") == 0:
    print("Success, you may install tinbrow now!")
    exit(0)
else:
    print("Sadly, this script is not supported for your distro (e.g your using old version of debian)")
    exit(1)